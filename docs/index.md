# Python에서 고급 OOP 개념 마스터하기: OOP의 배경이 되는 이론 <sup>[1](#footnote_1)</sup>

이 포스팅에서는 Python의 고급 클래스 기능에 대한 탐색을 바탕으로 객체 지향 프로그래밍(OOP)의 이론적 기초를 살펴본다. 상속, 캡슐화, 다형성, 구성, 추상화, 집계, 연관 등과 같은 필수 OOP 개념과 Python에서의 실제 적용 사례를 살펴본다. 또한 메서드 해결 순서(MRO)와 주요 디자인 패턴을 살펴봄으로써 Python 프로그래밍 기술을 향상시킬 수 있는 이해를 포괄적으로 제공한다.

<a name="footnote_1">1</a>: [Mastering Advanced OOP Concepts in Python: Theory Behind OOP](https://blog.devgenius.io/mastering-advanced-oop-concepts-in-python-theory-behind-oop-c9e87fb1697b)를 편역한 것이다.

# Python에서 고급 OOP 개념 마스터하기: OOP의 배경이 되는 이론

## OOP의 기둥
Python의 객체 지향 프로그래밍은 추상화, 상속, 캡슐화 및 다형성의 네 기본 개념을 기반으로 한다. 또한 구성, 추상화, 집계 및 연관도 언급할 가치가 있다. 이러한 기둥은 단순한 이론적 구성이 아니라 Python 코드를 작성하고 구성하는 방식을 형성하는 실용적인 도구이다.

### 추상화
추상화란 특정 인스턴스보다는 무언가의 본질적인 특성에 집중하는 것이다. OOP에서는 객체가 어떻게 동작하는지가 아니라 무엇을 하는지에 집중하는 것을 의미한다.

이는 일반적으로 추상 클래스와 메서드를 사용하여 달성할 수 있다. 추상 클래스는 자체적으로 인스턴스화할 수 없으며 추상 메서드에 대한 구현을 제공하기 위해 서브클래스가 필요한 클래스이다.

```python
from abc import ABC, abstractmethod


class AbstractVehicle(ABC):
    @abstractmethod
    def move(self):
        pass


class Car(AbstractVehicle):
    def move(self):
        print("Car is moving")

# AbstractVehicle cannot be instantiated,
# but Car, which implements the abstract method, can.
```

### 상속
상속은 자식 클래스라고 하는 새 클래스가 기존 클래스인 부모 클래스로부터 속성과 동작(메서드)을 파생하는 메커니즘이다. 이 개념은 계층 구조를 만들고 클래스 간에 코드를 공유하는 데 핵심적인 역할을 한다.

```python
class Animal:
    def __init__(self, name):
        self.name = name

    def speak(self):
        pass


class Dog(Animal):
    def speak(self):
        return f"{self.name} says Woof!"


class Cat(Animal):
    def speak(self):
        return f"{self.name} says Meow!"


dog = Dog("Buddy")
cat = Cat("Whiskers")
print(dog.speak())  # Output: Buddy says Woof!
print(cat.speak())  # Output: Whiskers says Meow!
```

상속은 명확한 "is-a" 관계를 모델링하는 데 가장 잘 사용된다. 'has-a' 관계가 더 적절한 경우에는 부적절한 결합으로 이어질 수 있으므로 피하여야 한다. 상속 계층 구조가 깊으면 관리하고 이해하기 어려울 수 있으므로 주의하도록 한다. 억지로 끼워 맞추는 경우 상속보다는 구성(composition)이 선호된다.

```python
class Account:
    def __init__(self, id, balance=0):
        self.__id = id        # Private attribute
        self.__balance = balance

    def deposit(self, amount):
        if amount > 0:
            self.__balance += amount

    def withdraw(self, amount):
        if 0 < amount <= self.__balance:
            self.__balance -= amount
            return amount
        return "Insufficient balance"

    def get_balance(self):
        return self.__balance


account = Account(1, 1000)
account.deposit(500)
print(account.get_balance())  # 1500
account.withdraw(200)
print(account.get_balance())  # 1300
```

비공개 속성을 액세스하고 수정할 때는 항상 getter와 setter 메서드를 사용하도록 한다. 필요한 경우가 아니면 내부 상태를 노출하지 마세요. 이 접근 방식은 데이터를 보호할 뿐만 아니라 향후 구현을 더 쉽게 변경할 수 있게 해준다.

### 다형성(Polymorphism)
다형성을 사용하면 서로 다른 클래스의 객체를 공통 수퍼클래스의 객체로 취급할 수 있다. 다형성은 단일 인터페이스를 사용하여 서로 다른 기본 형식(데이터 타입)을 표현하는 것이다.

```python
class Rectangle:
    def draw(self):
        return "Drawing a rectangle"

class Circle:
    def draw(self):
        return "Drawing a circle"

def draw_shape(shape):
    print(shape.draw())


draw_shape(Rectangle())  # Output: Drawing a rectangle
draw_shape(Circle())     # Output: Drawing a circle
```

다형성은 공통 인터페이스를 준수하는 유연하고 상호 교환 가능한 객체를 만드는 데 필수이다. 이를 통해 코드를 보다 모듈화하고 쉽게 확장할 수 있다.

### 구성(Composition)
구성은 기본 클래스나 부모 클래스에서 상속하는 것이 아니라 더 단순한 객체에서 복잡한 객체를 구성하는 것이다. 구성은 서로 다른 부분을 연결하여 시스템을 유연하게 만들 수 있다.

```python
class Battery:
    def charge(self):
        return "Battery charging"

class Smartphone:
    def __init__(self):
        self.battery = Battery()

    def charge_phone(self):
        return self.battery.charge()


phone = Smartphone()
print(phone.charge_phone())  # Output: Battery charging
```

구성은 여러 소스의 기능으로부터 객체를 구성해야 하거나 상속 관계가 복잡하여 실제 세계를 정확하게 모델링하지 못하는 경우에 적합하다. 느슨한 결합과 더 나은 캡슐화를 촉진하여 더 유지 관리가 쉽고 유연한 코드를 만들 수 있다.

### 집계(Aggregation)
집계는 객체 간의 "has-a" 관계를 나타내는 특수한 형태의 연결로, 자식 객체가 부모 객체와 독립적으로 존재할 수 있다. 일반적으로 whole-part 관계를 모델링하는 데 사용한다.

집계는 컴퓨터 시스템의 컴포넌트처럼 컴포넌트가 서로 다른 시스템에 속하거나 부모 객체와 독립적인 라이프사이클을 가질 수 있는 시나리오에서 유용하다.

```python
class Engine:
    def start(self):
        print("Engine starts")


class Car:
    def __init__(self, engine):
        self.engine = engine

    def start(self):
        self.engine.start()


engine = Engine()
car = Car(engine)
car.start()  # The Engine is part of the Car, but it's a separate, independent object
```

### 연관(Association)
연관은 한 개체가 다른 개체 내에서 사용되는 것을 포함하여 객체 간의 광범위한 관계를 나타낸다. 연결은 집계와 달리 소유권을 의미하지 않는다.

연관은 데이터베이스 관계와 학교 시스템에서 학생과 교사 간의 관계처럼 객체가 상호 작용해야 하지만 독립성과 수명 주기를 유지해야 하는 시나리오에서 자주 사용된다.

```python
class Professor:
    pass


class Department:
    def __init__(self, professor):
        self.professor = professor


# Here, Department is associated with Professor,
# but neither owns the other.
```

## Python의 메서드 해결 순서(Method Resolution Order)
복잡한 상속 구조를 다룰 때는 Python의 메서드 해결 순서(MRO)를 이해하는 것이 중요합니다. 메서드를 실행할 때 베이스 클래스가 검색되는 순서를 결정합니다. MRO를 제대로 이해하면 객체 지향 설계, 특히 다중 상속 시나리오에서 흔히 발생하는 함정을 방지할 수 있습니다.

### MRO의 개념
MRO는 Python이 메서드 해석 중에 베이스 클래스를 검색할 순서를 결정하기 위해 따르는 규칙이다. 이는 클래스가 둘 이상의 부모 클래스로부터 상속되는 다중 상속에서 특히 중요하다.

### Python의 MRO 알고리즘
파이썬은 MRO에 C3 선형화 알고리즘을 사용한다. 이 알고리즘은 메서드 해결을 위한 일관되고 예측 가능한 구조를 제공하여 메서드가 반복되지 않고 각 부모가 한 번만 고려되도록 보장한다.

### 예를 통한 MRO 이해

```python
class A:
    def do_something(self):
        print("Method Defined In: A")


class B(A):
    def do_something(self):
        print("Method Defined In: B")


class C(A):
    def do_something(self):
        print("Method Defined In: C")


class D(B, C):
    pass


d = D()
d.do_something()  # Output: Method Defined In: B
```

위의 예에서 `d.do_something()`을 호출할 때 Python은 `D`, `B`, `C`, `A`로 구성된 `D` 클래스의 MRO를 따르며, `B` 클래스에서 찾은 첫 번째 `do_something()` 메서드를 실행한다.

### MRO와 `super()`
`super()` 함수는 부모 클래스의 메서드를 호출하는 데 사용된다. MRO의 컨텍스트에서 `super()`는 MRO를 따라 메서드를 찾을 다음 클래스를 결정한다.

```python
class Base:
    def __init__(self):
        print("Base initializer")


class A(Base):
    def __init__(self):
        super().__init__()
        print("A initializer")


class B(Base):
    def __init__(self):
        super().__init__()
        print("B initializer")


class C(A, B):
    def __init__(self):
        super().__init__()
        print("C initializer")


c = C()
```

이 코드의 출력은 클래스 `C`의 MRO에 따라 이니셜라이저가 호출되는 순서를 보여준다.

### 모범 사례와 일반적인 함정

- **다이아몬드 상속 피하기**: "다이아몬드 문제"는 한 클래스가 공통 기본 클래스를 가진 두 클래스로부터 상속받을 때 여러 상속에서 발생한다. MRO를 올바르게 이해하고 적용하면 이러한 복잡성을 해결하는 데 도움이 될 수 있다.
- **복잡한 상속보다 컴포지션을 선호**: 상속 구조가 너무 복잡해지면 관계를 단순화하고 코드 가독성을 개선하기 위한 대안으로 구성을 사용하는 것을 고려하자.
- **`super()`의 일관된 사용**: 부모 메서드를 재정의하는 모든 메서드에서 `super()`가 일관되게 사용되는지 확인하자. 이렇게 하면 MRO의 무결성을 유지하고 예기치 않은 동작을 방지하는 데 도움이 된다.

Python의 MRO는 강력한 기능이지만 객체 지향 설계에서 복잡성을 피하기 위해 신중한 처리가 필요하다. MRO의 원리를 올바르게 이해하고 적용하면 특히 복잡한 상속 시나리오에서 코드의 견고성과 명확성을 크게 향상시킬 수 있다.

## Python의 디자인 패턴
디자인 패턴은 소프트웨어 개발자에게 필수적인 도구로, 소프트웨어 설계의 일반적인 문제에 대한 검증된 솔루션을 제공한다. Python에서는 특정 패턴이 자주 사용되며 실용성이 뛰어나다. 가장 유용한 몇몇 패턴에 초점을 맞춰 어떻게 작동하고 어디에 효과적으로 적용할 수 있는지 설명하겠다.

### 싱글톤 패턴(생성)

- **목적**: 클래스에 인스턴스가 하나만 있는지 확인하고 클래스에 대한 전역 액세스 지점을 제공한다.
- **작동 방식**: 싱글톤 패턴은 클래스의 인스턴스화를 하나의 객체로 제한한다. 이는 자신의 인스턴스가 이미 존재하는지 확인하고 존재하지 않으면 새로 생성하는 클래스를 생성하여 구현된다.
- **사용 사례**: 싱글톤은 단일 제어 지점이 필요한 구성, 로깅, 데이터베이스 연결, 파일 관리자에서 자주 사용된다.

```python
class Singleton:
    _instance = None

    @classmethod
    def getInstance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance


# Usage
s1 = Singleton.getInstance()
s2 = Singleton.getInstance()
assert s1 is s2  # Both are the same instance
```

### Factory 메소드 패턴(생성)

- **목적**: 수퍼클래스에서 객체를 생성하기 위한 인터페이스를 제공하지만, 서브클래스가 생성될 객체의 타입을 변경할 수 있도록 한다.
- **작동 방식**: Factory 메서드 패턴은 객체 생성을 위한 인터페이스를 정의하지만 인스턴스화는 서브클래스에 위임하는 방식으로 작동한다. 이 패턴은 구성 프로세스가 복잡할 때 특히 유용하다.
- **사용 사례**: 사용자가 선호하는 설정에 따라 다른 UI 요소를 만들고, 지역에 따라 다른 결제 처리 방법을 만들 수 있다.

```python
class Polygon:
    def __init__(self, sides):
        self.sides = sides


class Triangle(Polygon):
    # Implementation specific to Triangle


class Square(Polygon):
    # Implementation specific to Square


class PolygonFactory:
    @staticmethod
    def get_polygon(sides):
        if sides == 3:
            return Triangle(sides)
        if sides == 4:
            return Square(sides)
        raise ValueError("No such polygon")


# Usage
triangle = PolygonFactory.get_polygon(3)
square = PolygonFactory.get_polygon(4)
```

### 관찰자 패턴(행동)

- **목적**: 객체가 다른 객체(관찰자)에게 자신의 상태 변화를 알릴 수 있도록 한다.
- **작동 방식**: 관찰자 패턴은 주체가 관찰자 목록을 유지 관리하고 상태 변경 시 관찰자에게 알리는 게시-구독(publish-subscribe) 모델이다. 이 패턴은 분산 이벤트 처리 시스템을 구현하는 데 매우 중요하다.
- **사용 사례**: 이벤트 처리 시스템, 데이터 브로드캐스팅을 구현하거나 주식 시장 업데이트와 같은 실시간 데이터 피드를 구현하는 데 사용된다.

```python
class NewsPublisher:
    def __init__(self):
        self._subscribers = []
        self._latest_news = None

    def attach(self, subscriber):
        self._subscribers.append(subscriber)

    def detach(self, subscriber):
        self._subscribers.remove(subscriber)

    def notify_subscribers(self):
        for subscriber in self._subscribers:
            subscriber.update()

    def add_news(self, news):
        self._latest_news = news
        self.notify_subscribers()

    def get_news(self):
        return self._latest_news


class Subscriber:
    def update(self):
        # Get the latest news from the publisher
        pass


# Usage
publisher = NewsPublisher()
subscriber1 = Subscriber()
publisher.attach(subscriber1)
publisher.add_news("Breaking News: Python 4.0 Released!")
```

### 전략 패턴(행동)

- **목적**: 런타임에 알고리즘의 구현을 선택할 수 있다.
- **작동 방식**: 전략 패턴은 알고리즘 제품군을 정의하고, 각 알고리즘을 캡슐화하며, 상호 교환 가능하게 만드는 작업을 포함한다. 알고리즘은 알고리즘을 사용하는 클라이언트에 따라 독립적으로 달라질 수 있다.
- **사용 사례**: 전략 패턴은 다양한 데이터 압축 또는 암호화 방법과 같이 상황에 따라 알고리즘을 동적으로 전환해야 하는 시나리오에 유용하다.

```python
from abc import ABC, abstractmethod


class CompressionStrategy(ABC):
    @abstractmethod
    def compress(self, files):
        pass


class ZipCompressionStrategy(CompressionStrategy):
    def compress(self, files):
        # ZIP compression logic
        return "Compressed with ZIP"


class RarCompressionStrategy(CompressionStrategy):
    def compress(self, files):
        # RAR compression logic
        return "Compressed with RAR"


class Compressor:
    def __init__(self, strategy: CompressionStrategy):
        self.strategy = strategy

    def compress_files(self, files):
        return self.strategy.compress(files)


 # Usage
 files = ["file1.txt", "file2.jpg"]
 zip_compressor = Compressor(ZipCompressionStrategy())
 rar_compressor = Compressor(RarCompressionStrategy())
 print(zip_compressor.compress_files(files))  # Compressed with ZIP
 print(rar_compressor.compress_files(files))  # Compressed with RAR
```

### 어댑터 패턴(구조적)

- **목적**: 호환되지 않는 인터페이스가 함께 작동할 수 있도록 한다.
- **작동 방식**: 어댑터 패턴은 호환되지 않는 두 인터페이스 사이의 다리 역할을 한다. 이 패턴에는 독립적이거나 호환되지 않는 인터페이스의 기능을 결합하는 단일 클래스인 어댑터가 포함된다.
- **사용 사례**: 일반적으로 소프트웨어 라이브러리와 프레임워크에서 사용되며, 한 클래스에서 제공하는 기능을 나머지 시스템과 호환되도록 만들어야 한다.

```python
class EuropeanSocketInterface:
    def voltage(self):
        pass


class AmericanSocketInterface:
    def voltage(self):
        pass


class EuropeanSocket(EuropeanSocketInterface):
    def voltage(self):
        return 230


class AmericanSocket(AmericanSocketInterface):
    def voltage(self):
        return 110


class SocketAdapter(EuropeanSocketInterface):
    def __init__(self, socket):
        self.socket = socket

    def voltage(self):
        return self.socket.voltage()


# Usage
american_socket = AmericanSocket()
adapter = SocketAdapter(american_socket)
print(f"Adapter voltage: {adapter.voltage()}V")  # Adapter voltage: 110V
```

### 프록시 패턴(구조적)

- **목적**: 다른 객체에 대한 접근을 제어할 수 있는 대리자 또는 플레이스홀더를 제공한다. 지연 로딩, 액세스 제어 또는 로깅 등에 유용하다.
- **작동 방식**: 프록시 패턴에서 프록시 객체는 실제 객체와 인터페이스하는 데 사용된다. 프록시는 실제 객체에 대한 호출을 가로채서 지연 초기화, 액세스 제어 또는 로깅과 같은 추가 작업을 추가할 수 있다.
- **사용 사례**: 프록시 패턴은 네트워크 어플리케이션에서 클라이언트와 서버 사이에 계층을 추가하기 위해 널리 사용된다(예: 원격 메서드 호출 처리). 또한 객체 생성이 리소스 집약적이어서 객체가 실제로 필요할 때까지 지연시키고자 하는 시나리오에서도 유용하다.

```python
class Image:
    def display(self):
        pass


class RealImage(Image):
    def __init__(self, filename):
        self.filename = filename
        self.load_from_disk()

    def load_from_disk(self):
        print(f"Loading {self.filename}")

    def display(self):
        print(f"Displaying {self.filename}")


class ProxyImage(Image):
    def __init__(self, filename):
        self.filename = filename
        self.real_image = None

    def display(self):
        if self.real_image is None:
            self.real_image = RealImage(self.filename)
        self.real_image.display()


# Usage
image = ProxyImage("test_image.jpg")
image.display()  # Loads and displays image
image.display()  # Only displays image, as it's already loaded
```

### 모범 사례와 일반적인 함정

- **적절한 사용**: 패턴이 문제에 맞는지 확인하여라. 디자인 패턴을 잘못 적용하면 불필요한 복잡성을 초래할 수 있다.
- **단순성 우선**: 가장 간단한 솔루션부터 시작하자. 코드가 복잡해지거나 패턴이 분명한 이점을 제공하는 경우 패턴으로 리팩터링하세요.
- **진화와 리팩토링**: 새로운 요구 사항이 등장하거나 현재 설계의 한계가 분명해지면 설계를 리팩토링할 준비를 하자.

Python의 디자인 패턴은 일반적인 디자인 문제를 우아하고 효율적인 방식으로 해결하기 위한 툴킷을 제공한다. 언제, 어떻게 사용해야 하는지 알면 코드의 품질과 유지보수성을 크게 향상시킬 수 있다.

## 기본을 넘어
OOP의 핵심 개념은 탄탄한 기초를 제공하지만, Python의 고급 기능을 사용하면 훨씬 더 강력하고 유연한 설계가 가능하다. 이 섹션에서는 Python 프로그래밍 기술을 크게 향상시킬 수 있는 이러한 정교한 기능 중 일부를 살펴본다.

### 메타클래스(Metaclasses)
Python에서 메타클래스는 '클래스의 클래스'이다. 메타클래스는 클래스의 동작 방식을 정의한다. 메타클래스는 클래스가 인스턴스에 해당하는 개념이다. 메타클래스는 특정 특성이나 동작을 가진 클래스를 만드는 데 사용된다.

모든 클래스가 특정 메서드 또는 속성 집합을 가져야 하는 프레임워크를 상상해 보자. 메타클래스는 이러한 속성을 자동으로 추가하거나 규칙을 적용하여 프레임워크 전체에 일관성을 보장할 수 있다.

```python
class UniformMeta(type):
    def __new__(cls, name, bases, dct):
        # Ensure each class has a required_method
        assert 'required_method' in dct, "Missing required_method"
        return super().__new__(cls, name, bases, dct)


class MyClass(metaclass=UniformMeta):
    def required_method(self):
        pass
```

이 접근 방식은 여러 모듈이나 플러그인에서 일관된 동작이 중요한 대규모 프레임워크에서 자주 볼 수 있다.

### 종속성 주입(Dependency Injection)
종속성 주입(DI)은 종속성을 하드 코딩하지 않고 객체에 주입하여 느슨한 결합을 촉진하고 테스트를 쉽게 하는 기술이다.

웹 어플리케이션에서 데이터베이스 액세스를 담당하는 서비스 클래스를 컨트롤러 클래스에 주입하면 데이터베이스 백엔드를 쉽게 교체하거나 테스트에서 데이터베이스 계층을 모킹할 수 있다.

```python
class DatabaseService:
    def get_data(self):
        return "Data from database"


class UserController:
    def __init__(self, db_service):
        self.db_service = db_service

    def handle_request(self):
        data = self.db_service.get_data()
        return data


# Usage
db_service = DatabaseService()
user_controller = UserController(db_service)
```

종속성 주입은 유연성과 테스트 용이성 때문에 최신 웹 프레임워크와 대규모 어플리케이션에서 광범위하게 사용된다.

## 마치며
Python의 객체 지향 프로그래밍을 살펴보면서 상속, 캡슐화, 다형성, 구성 등과 같은 핵심 요소부터 추상화, 집계, 연관성의 미묘한 원칙에 이르기까지 필수적인 개념을 살펴보았다. 또한 메서드 해결 순서(MRO)의 복잡성을 풀고 Python에서 가장 유용한 디자인 패턴을 파헤쳐 실제 프로그래밍 문제를 해결하는 데 실제 적용하는 방법을 보여였다.

한 걸음 더 나아가 메타클래스와 종속성 주입과 같은 정교한 기능도 다루었다. 이러한 고급 주제는 Python의 기능에 대한 이해를 높일 뿐만 아니라 더 효율적이고 적응력 있는 코드를 작성할 수 있는 새로운 길을 열어준다.

Python의 OOP를 마스터하는 여정은 계속 진화하고 있다. 이러한 개념을 프로그래밍 프로젝트의 혁신과 개선을 위한 도구로 활용하세요. 기능적일 뿐만 아니라 우아하게 설계되고 유지 관리가 가능한 소프트웨어를 만드는 데 도움이 될 것이다. 계속 실험하고 배우다 보면 Python OOP 기술이 발전하는 것을 볼 수 있을 것이다.

